# karate_builder.py
Build a Karate test feature file from an EHI test data spreadsheet

## Setup
```console
$ pip install -r requirements.txt
```

## Usage
```console
$ python karate_builder.py -h
usage: karate_builder.py [-h] [-o OUTFILE] [-u] [--scope SCOPE] [--header HEADER] [--endpoint ENDPOINT]
                         [--id-prefix ID_PREFIX] [--name NAME] [--header-init HEADER_INIT]
                         [--include-default-headers] [-X EXCLUDE] [--url-prefix URL_PREFIX] [-t TAB]
                         [-p PLACEHOLDER]
                         spreadsheet

Build a Karate test feature file from an EHI test data spreadsheet

positional arguments:
  spreadsheet           test data spreadsheet

optional arguments:
  -h, --help            show this help message and exit
  -o OUTFILE, --outfile OUTFILE
                        output file (default: print to stdout)
  -u, --dynamic-urn     if set, all URNs will be changed to calls to a dynamic urn-builder named "urn" which must
                        be supplied by the Karate environment
  --scope SCOPE         index of 'Scope' sheet (default: 0)
  --header HEADER       index of 'HeaderTestDetails' sheet (default: 1)
  --endpoint ENDPOINT   index of 'EndpointTestDetails' sheet (default: 2)
  --id-prefix ID_PREFIX
                        scenario ID prefix string (default: TC)
  --name NAME           feature name (default: TODO)
  --header-init HEADER_INIT
                        code to initialize default headers (default: no default headers are used)
  --include-default-headers
                        if the --header-init argument is set, this will force Scenarios to include default values
                        of headers
  -X EXCLUDE, --exclude EXCLUDE
                        exclude a header from the generated test scenarios, even if it is present in the test
                        spreadsheet
  --url-prefix URL_PREFIX
                        url path segment to insert before the endpoint URL
  -t TAB, --tab TAB     string to use for a single level of indentation (default: two spaces)
  -p PLACEHOLDER, --placeholder PLACEHOLDER
                        string used as a placeholder wherever a value must be manually provided (default: TODO)
```
