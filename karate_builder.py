#! /usr/bin/env python3.8
"""Build a Karate test feature file from an EHI test data spreadsheet"""
#
# Setup:
# $ pip install pandas==1.3.5 openpyxl==3.0.9
#
# Example usage:
# $ python karate_builder.py "Search Damage History Test Cases.xlsx" \
#    --name "Search Damage History" \
#    --url-prefix "/vrservices" \
#    --header-init "call read('classpath:com/ehi/vrservices/karate/validHeaders.js') {jwtToken: '#(jwtToken)'}" \
#    --dynamic-urn \
#    -o SearchDamageHistory.feature

import sys
import json
import re
import argparse
import itertools
from pathlib import Path
import pandas as pd

import typing as T

try:
    # if bpython is installed, use as debugger
    import bpdb
except ImportError:
    bpdb = None

PLACEHOLDER_DEFAULT = "TODO"

SCOPE_SHEET_DEFAULT = 0
HEADER_TEST_SHEET_DEFAULT = 1
ENDPOINT_TEST_SHEET_DEFAULT = 2
SCENARIO_ID_COLUMN = 1
SCENARIO_ID_PREFIX_DEFAULT = "TC"
JWT_TOKEN_KEY = "{{AppSec_JWT}}"
JWT_TOKEN_VALUE = "#(jwtToken)"
SPAN_KEY = "SpanID{{sessionId}}"
TRACE_KEY = "TraceID{{sessionId}}"
URL_VARIABLE = "apiUrl"
HEADER_VARIABLE = "defaultHeaders"

FEATURE_NAME_DEFAULT = PLACEHOLDER_DEFAULT
METHOD_DEFAULT = "GET"
STATUS_DEFAULT = 200
DECORATORS_DEFAULT = ["@integration"]
LOCALE_DEFAULT = "eng-USA"
REGION_DEFAULT = "NORTH_AMERICA"
CALLING_APPLICATION_DEFAULT = "Karate"
MEDIA_TYPE_DEFAULT = "application/json"
SUPPORTED_LOCALES_DEFAULT = [
    "deu-DEU",
    "eng-CAN",
    "eng-GBR",
    "eng-IRL",
    "eng-USA",
    "fra-CAN",
    "fra-FRA",
    "ita-ITA",
    "nld-NLD",
    "por-BRA",
    "por-PRT",
    "spa-ESP",
]
HEADER_ACCEPT = "Accept"
HEADER_CONTENT_TYPE = "Content-Type"
HEADER_AUTHORIZATION = "Authorization"
HEADER_LOCALE = "Ehi-Locale"
HEADER_REGION = "Ehi-Region"
HEADER_CALLING_APPLICATION = "Ehi-Calling-Application"
HEADER_ORIGIN_IDENTITY = "Ehi-Origin-Identity"
HEADER_SPAN = "X-B3-SpanId"
HEADER_TRACE = "X-B3-TraceId"

# TODO this should be configurable
HEADER_DEFAULTS = {
    HEADER_ACCEPT: MEDIA_TYPE_DEFAULT,
    HEADER_CONTENT_TYPE: MEDIA_TYPE_DEFAULT,
    HEADER_AUTHORIZATION: JWT_TOKEN_KEY,
    HEADER_LOCALE: LOCALE_DEFAULT,
    HEADER_REGION: REGION_DEFAULT,
    HEADER_CALLING_APPLICATION: CALLING_APPLICATION_DEFAULT,
    HEADER_SPAN: SPAN_KEY,
    HEADER_TRACE: TRACE_KEY,
}
HEADERS = set(HEADER_DEFAULTS.keys())

# I think the column names are just picked arbitrarily.
# Update this manually to include new column names.
# Would be cool if this was configurable through the CLI but could be awkward
COLUMN_NAME_MAP = {
    "ScenarioID": "id",
    "TestType": "type",
    "request Method": "method",
    "requestMethod": "method",
    "Ehi-Locale": HEADER_LOCALE,
    "Ehi-locale": HEADER_LOCALE,
    "RequestURL": "url",
    "RequestBody": "request",
    "Expected_HttpResponseCode": "status",
    "Expected_StatusCode": "status",
    "Expected_ErrorCode": "error_code",
    "Expected_ErrorMessage": "response_body",
    "ExpectedResponse": "response_body",
    "AppSec_Token": HEADER_AUTHORIZATION,
    "Accept": HEADER_ACCEPT,
    "contentType": HEADER_CONTENT_TYPE,
    "ehiCallingApplication": HEADER_CALLING_APPLICATION,
    "ehiRegion": HEADER_REGION,
    "Ehi-region": HEADER_REGION,
    "ehiOriginIdentity": HEADER_ORIGIN_IDENTITY,
    "ehiSpanId": HEADER_SPAN,
    "ehiTraceId": HEADER_TRACE,
}

# two-space tabs
TAB_STRING_DEFAULT = "  "

# dynamic URN restructuring
DYNAMIC_URN_PATTERN = re.compile(
    "(?P<q>['\"])urn:com.ehi:(?P<env>\\w+):(?P<path>.*?)(?P=q)"
)
DYNAMIC_URN_SUB = "'#(urn(\"\\g<path>\"))'"


def _get_nansafe(series: pd.Series, key: str, default: T.Any = None) -> T.Any:
    """Works like ``Series.get`` but the default is also used in place of NaN."""
    if key in series:
        value = series[key]
        if not pd.isna(value):
            return value
    return default


def _parenthetical(message: str, parenthetical: T.Optional[str]) -> str:
    """Format a message with an optional parenthetical."""
    if parenthetical is not None:
        return f"{message} ({parenthetical})"
    else:
        return message


def _build_karate_test(
    scenario_id: T.Optional[str] = None,
    scenario_desc: T.Optional[str] = None,
    preamble: T.Optional[T.List[str]] = None,
    url: T.Optional[str] = None,
    header_variable: T.Optional[str] = None,
    headers: T.Optional[T.Dict[str, T.Optional[str]]] = None,
    request: T.Optional[str] = None,
    method: str = METHOD_DEFAULT,
    status: int = STATUS_DEFAULT,
    match: T.Optional[T.Dict[str, str]] = None,
    match_contains: T.Optional[T.Dict[str, str]] = None,
    tab_string: str = TAB_STRING_DEFAULT,
    placeholder: str = PLACEHOLDER_DEFAULT,
) -> T.Iterator[str]:
    """Build and format a Karate test."""
    # populate defaults
    if scenario_desc is None:
        scenario_desc = placeholder
    if url is None:
        url = placeholder
    if request is None:
        request = placeholder
    headers = headers or {}
    match = match or {}
    match_contains = match_contains or {}

    # Generate line-by-line
    name = scenario_desc if scenario_desc is not None else "TODO"
    if scenario_id is not None:
        name = f"{name} ({scenario_id})"
    yield f"Scenario: {name}"
    for line in preamble or []:
        yield tab_string + f"* {line}"
    yield tab_string + f"Given url {url}"
    if header_variable is not None:
        yield tab_string + f"And headers {header_variable}"
    for header, value in headers.items():
        yield tab_string + f"And header {header} = {json.dumps(value)}"
    yield tab_string + f"And request {request}"
    yield tab_string + f"When method {method}"
    yield tab_string + f"Then status {status}"
    for key, value in match.items():
        yield tab_string + f"And match {key} == {value}"
    for key, value in match_contains.items():
        yield tab_string + f"And match {key} contains {value}"


def _transform_row(
    row: pd.Series,
    scenario_id: str,
    scenario_desc: str,
    scenario_notes: T.Optional[str],
    header_variable: T.Optional[str] = None,
    exclude_headers: T.Optional[T.List[str]] = None,
    include_default_headers: bool = False,
    url_path_prefix: T.Optional[str] = None,
    dynamic_urn: bool = False,
    placeholder: str = PLACEHOLDER_DEFAULT,
    **kwargs,
) -> T.Iterator[str]:
    # Populate default options
    exclude_headers = exclude_headers or []
    url_path_prefix = url_path_prefix or ""

    # default headers are used when the column is not present
    # a blank value indicates the header should be absent
    headers: T.Dict[str, T.Optional[str]] = {}
    for k in HEADERS:
        if k in row:
            if pd.isna(row[k]):
                if k == HEADER_CONTENT_TYPE:
                    # Karate inserts default content-type unless an empty string is used
                    headers[k] = ""
                else:
                    # manually set to null
                    # this can be used to override a header with a default value
                    headers[k] = None
            else:
                headers[k] = row[k]

    if header_variable is not None and not include_default_headers:
        # omit headers that match the defaults
        headers = {k: v for k, v in headers.items() if HEADER_DEFAULTS[k] != v}

    if headers.get(HEADER_AUTHORIZATION) == JWT_TOKEN_KEY:
        # resolve (well, kind of) the jwt token key
        headers[HEADER_AUTHORIZATION] = f"Bearer {JWT_TOKEN_VALUE}"

    for k in exclude_headers:
        if k in headers:
            del headers[k]

    request = row["request"]
    if dynamic_urn:
        # transform URNs
        request = DYNAMIC_URN_PATTERN.sub(DYNAMIC_URN_SUB, request)

    # build expected response values
    error_code = _get_nansafe(row, "error_code")
    if error_code:
        # if an error code is given, assume standard EHI error response
        match = None
        response = {
            "code": error_code,
            "locale": headers.get(HEADER_LOCALE, LOCALE_DEFAULT),
            "localizedMessage": _get_nansafe(
                row, "response_body", _parenthetical(placeholder, scenario_notes)
            ),
            "severity": "ERROR",  # TODO can this ever not be ERROR?
        }
        if response["locale"] not in SUPPORTED_LOCALES_DEFAULT:
            response["locale"] = LOCALE_DEFAULT
        if row["status"] == 400:
            # validation errors usually contain paths
            response["paths"] = [placeholder]
        match_contains = {"response.errors": json.dumps(response)}
    else:
        # if no error code is given, we don't know what the response looks like
        match_contains = None
        match = {"response": _parenthetical(placeholder, scenario_notes)}

    yield from _build_karate_test(
        scenario_id=scenario_id,
        scenario_desc=scenario_desc,
        url=f"{URL_VARIABLE} + {json.dumps(url_path_prefix + row['url'])}",
        header_variable=header_variable,
        headers=headers,
        request=request,
        method=row["method"],
        status=row["status"],
        match=match,
        match_contains=match_contains,
        placeholder=placeholder,
        **kwargs,
    )


def _generate_spreadsheet(
    spreadsheet: T.Union[str, bytes, Path, T.IO],
    scope_idx: int = SCOPE_SHEET_DEFAULT,
    header_idx: int = HEADER_TEST_SHEET_DEFAULT,
    endpoint_idx: int = ENDPOINT_TEST_SHEET_DEFAULT,
    feature_name: str = FEATURE_NAME_DEFAULT,
    feature_decorators: T.List[str] = DECORATORS_DEFAULT,
    header_init: T.Optional[str] = None,
    id_prefix: str = SCENARIO_ID_PREFIX_DEFAULT,
    tab_string: str = TAB_STRING_DEFAULT,
    placeholder: str = PLACEHOLDER_DEFAULT,
    **kwargs,
) -> T.Iterator[str]:
    """Generator version of ``convert_spreadsheet``"""

    # read scenario descriptions from Scope sheet
    scenarios = pd.read_excel(
        spreadsheet,
        sheet_name=scope_idx,
        names=["description", "id", "notes"],
        index_col=SCENARIO_ID_COLUMN,
    ).filter(like=id_prefix, axis=0)

    header_tests = (
        pd.read_excel(
            spreadsheet,
            sheet_name=header_idx,
            index_col=0,
        )
        .rename(columns=COLUMN_NAME_MAP)
        .filter(like=id_prefix, axis=0)
    )

    endpoint_tests = (
        pd.read_excel(
            spreadsheet,
            sheet_name=endpoint_idx,
            index_col=0,
        )
        .rename(columns=COLUMN_NAME_MAP)
        .filter(like=id_prefix, axis=0)
    )

    # generate file line-by-line
    yield from feature_decorators
    yield f"Feature: {feature_name}"
    yield ""
    yield tab_string + "Background:"
    yield tab_string * 2 + f"* url {URL_VARIABLE}"
    header_variable = None
    if header_init is not None:
        header_variable = HEADER_VARIABLE
        yield tab_string * 2 + f"* def {header_variable} = {header_init}"
    yield ""

    for id, row in itertools.chain(endpoint_tests.iterrows(), header_tests.iterrows()):
        desc = _get_nansafe(scenarios["description"], id, placeholder)
        notes = _get_nansafe(scenarios["notes"], id)
        for line in _transform_row(
            row,
            id,
            desc,
            notes,
            tab_string=tab_string,
            header_variable=header_variable,
            placeholder=placeholder,
            **kwargs,
        ):
            yield tab_string + line
        yield ""


def convert_spreadsheet(
    spreadsheet: T.Union[str, bytes, Path, T.IO],
    **kwargs,
) -> str:
    """Parse Karate tests from an EHI test data spreadsheet.

    Parameters
    ----------
    spreadsheet : str, bytes, :obj:`Path`, or file-like object, optional
        Excel spreadsheet file or path. See ``pandas.read_excel``
    scope_idx : int, optional
        Index of the 'Scope' sheet in the spreadsheet, defaults to 0.
        This sheet contains the notes & description associated with
        each test scenario.
    header_idx : int, optional
        Index of the 'HeaderTestDetails' sheet in the spreadsheet,
        defaults to 1. This sheet contains data for request header
        tests.
    endpoint_idx : int, optional
        Index of the 'EndpointTestDetails' sheet in the spreadsheet,
        defaults to 2. This sheet contains data for endpoint behavior
        tests.
    id_prefix : str, optional
        String prefix of Scenario IDs. Used for selecting rows
        containing test data.
    feature_name : str, optional
        Name of the feature covered by the tests in this feature file.
        Defaults to a placeholder value ("TODO")
    feature_decorators : list of str, optional
        List of decorators to be applied to this feature file. By
        default, this includes "@integration".
    header_init : str, optional
        Code to use to initialize default test headers. If given, this
        value will be assigned to a shared variable and used in all
        tests; otherwise, no default headers will be used.
    include_default_headers : bool, optional
        If ``header_init`` is provided, it is assumed that the file's
        header variable will be initialized with suitable defaults; in
        Scenarios where the input spreadsheet specifies a value of a
        header which is also the default, that value will be quietly
        omitted rather than explicitly (redundantly) included. This
        behavior can be overridden by setting this flag to ``True``,
        in which case all header values that match the defaults will
        be included.
    exclude_headers : list of str, optional
        The headers in this list will be excluded from the generated
        scenarios, even if they are present in the
        spreadsheet. Specifically, this means the values of these
        headers will not be explicitly set by a scenario.
    url_path_prefix : str, optional
        A URL path string to insert before endpoint URLs (which are
        scenario-dependent), and after the service hostname (which is
        karate-dependent). This is useful for services that run on an
        extended path, like in the case of a reverse-proxied service.
    dynamic_urn : bool, optional
        If ``True``, urns will be dynamically modified to be
        constructed by a dynamic URN builder called ``urn``, which
        must be supplied by the Karate environment. This is useful for
        building URNs for the current testing environment.
    tab_string : str, optional
        String to use for one level of indentation in the generated
        Karate feature file.
    placeholder : str, optional
        String inserted wherever required data must be manually inserted.
        Defaults to "TODO".
    preamble : list of str, optional
        A list of lines to be included as bare code at the beginning
        of each scenario.

    Returns
    -------
    str
        Body of the generated Karate test feature file
    """
    return "\n".join(_generate_spreadsheet(spreadsheet, **kwargs))


def _spool(args: argparse.Namespace, f: T.IO):
    try:
        for line in _generate_spreadsheet(
            args.spreadsheet,
            scope_idx=args.scope,
            header_idx=args.header,
            endpoint_idx=args.endpoint,
            id_prefix=args.id_prefix,
            feature_name=args.name,
            header_init=args.header_init,
            include_default_headers=args.include_default_headers,
            exclude_headers=args.exclude,
            url_path_prefix=args.url_prefix,
            dynamic_urn=args.dynamic_urn,
            tab_string=args.tab,
            placeholder=args.placeholder,
        ):
            print(line, file=f)
    except Exception as ex:
        print("Error processing spreadsheet", file=sys.stderr)
        if bpdb is not None:
            print("Entering debugger...", file=sys.stderr)
            bpdb.post_mortem()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, add_help=True)
    parser.add_argument("spreadsheet", type=str, help="test data spreadsheet")
    parser.add_argument(
        "-o",
        "--outfile",
        type=str,
        help="output file (default: print to stdout)",
        default=None,
    )
    parser.add_argument(
        "-u",
        "--dynamic-urn",
        action="store_true",
        help=(
            "if set, all URNs will be changed to calls to a dynamic "
            'urn-builder named "urn" which must be supplied by the '
            "Karate environment"
        ),
    )
    parser.add_argument(
        "--scope",
        type=int,
        help=f"index of 'Scope' sheet (default: {SCOPE_SHEET_DEFAULT})",
        default=SCOPE_SHEET_DEFAULT,
    )
    parser.add_argument(
        "--header",
        type=int,
        help=(
            "index of 'HeaderTestDetails' sheet "
            f"(default: {HEADER_TEST_SHEET_DEFAULT})"
        ),
        default=HEADER_TEST_SHEET_DEFAULT,
    )
    parser.add_argument(
        "--endpoint",
        type=int,
        help=(
            "index of 'EndpointTestDetails' sheet "
            f"(default: {ENDPOINT_TEST_SHEET_DEFAULT})"
        ),
        default=ENDPOINT_TEST_SHEET_DEFAULT,
    )
    parser.add_argument(
        "--id-prefix",
        type=str,
        help=f"scenario ID prefix string (default: {SCENARIO_ID_PREFIX_DEFAULT})",
        default=SCENARIO_ID_PREFIX_DEFAULT,
    )
    parser.add_argument(
        "--name",
        type=str,
        help=f"feature name (default: {FEATURE_NAME_DEFAULT})",
        default=FEATURE_NAME_DEFAULT,
    )
    parser.add_argument(
        "--header-init",
        type=str,
        help=(
            "code to initialize default headers "
            "(default: no default headers are used)"
        ),
    )
    parser.add_argument(
        "--include-default-headers",
        action="store_true",
        help=(
            "if the --header-init argument is set, this will force Scenarios "
            "to include default values of headers"
        ),
    )
    parser.add_argument(
        "-X",
        "--exclude",
        type=str,
        action="append",
        help=(
            "exclude a header from the generated test scenarios, "
            "even if it is present in the test spreadsheet"
        ),
    )
    parser.add_argument(
        "--url-prefix",
        type=str,
        help="url path segment to insert before the endpoint URL",
    )
    parser.add_argument(
        "-t",
        "--tab",
        type=str,
        help="string to use for a single level of indentation (default: two spaces)",
        default=TAB_STRING_DEFAULT,
    )
    parser.add_argument(
        "-p",
        "--placeholder",
        type=str,
        help=(
            "string used as a placeholder wherever a value must be manually provided "
            f"(default: {PLACEHOLDER_DEFAULT})"
        ),
        default=PLACEHOLDER_DEFAULT,
    )

    args = parser.parse_args()

    if args.outfile:
        with open(args.outfile, "w") as outfile:
            _spool(args, outfile)
    else:
        _spool(args, sys.stdout)
